# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Allan Nordhøy <epost@anotheragency.no>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: realmazharhussain@gmail.com\n"
"POT-Creation-Date: 2023-04-07 15:40+0500\n"
"PO-Revision-Date: 2023-03-22 14:41+0000\n"
"Last-Translator: Allan Nordhøy <epost@anotheragency.no>\n"
"Language-Team: Norwegian Bokmål <https://hosted.weblate.org/projects/gdm-"
"settings/language-names/nb_NO/>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.16.2-dev\n"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.appearance.gschema.xml:7
msgid "Theme Name"
msgstr "Draktnavn"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.appearance.gschema.xml:20
msgid "Type of Background"
msgstr "Bakgrunnstype"

#. Translators: GDM is an acronym for GNOME Display Manager (which is also GNOME's Login Manager).
#: data/gschemas/io.github.realmazharhussain.GdmSettings.appearance.gschema.xml:22
#, fuzzy
msgid "Whether GDM background should be an Image, Color or Default."
msgstr "Hvorvidt GDM-bakgrunnen skal være et bilde, helfarge, eller ingenting."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.appearance.gschema.xml:34
msgid "Background Image"
msgstr "Bakgrunnsbilde"

#. Translators: GDM is an acronym for GNOME Display Manager (which is also GNOME's Login Manager).
#: data/gschemas/io.github.realmazharhussain.GdmSettings.appearance.gschema.xml:36
msgid "The image file to use as GDM background"
msgstr "Bildefil å bruke som GDM-bakgrunn"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.appearance.gschema.xml:43
#: src/resources/ui/top-bar-page.blp:40
msgid "Background Color"
msgstr "Bakgrunnsfarge"

#. Translators: GDM is an acronym for GNOME Display Manager (which is also GNOME's Login Manager).
#: data/gschemas/io.github.realmazharhussain.GdmSettings.appearance.gschema.xml:45
#, fuzzy
msgid "The Color to use as GDM background"
msgstr "Helfarge å bruke som GDM-bakgrunn"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.mouse.gschema.xml:20
#: data/gschemas/io.github.realmazharhussain.GdmSettings.touchpad.gschema.xml:23
msgid "Pointer speed"
msgstr "Pekerhastighet"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.mouse.gschema.xml:21
#: data/gschemas/io.github.realmazharhussain.GdmSettings.touchpad.gschema.xml:24
msgid ""
"Pointer speed for the touchpad. Accepted values are in the [-1..1] range "
"(from “unaccelerated” to “fast”). A value of 0 is the system default."
msgstr ""
"Pekerhastighet for pekeflaten. Godkjente verdier er i området [-1..1] (fra "
"«uakselerert» til «rask»). «0» er systemforvalg."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.night-light.gschema.xml:7
msgid "If the night light mode is enabled"
msgstr "Hvis nattlysmodus er påslått"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.night-light.gschema.xml:8
msgid ""
"Night light mode changes the color temperature of your display when the sun "
"has gone down or at preset times."
msgstr ""
"Nattlysmodus endrer din skjerms fargetemperatur når solen har gått ned, "
"eller til forhåndsinnstilte tider."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.night-light.gschema.xml:13
#, fuzzy
msgid "Temperature of the display when enabled"
msgstr "Skjermens fargetemperatur når påslått"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.night-light.gschema.xml:14
msgid ""
"This temperature in Kelvin is used to modify the screen tones when night "
"light mode is enabled. Higher values are bluer, lower redder."
msgstr ""
"Denne temperaturen i Kelvin brukes til å endre skjermtonene når nattlys er "
"påslått. Høyere verdier er blåere, lavere rødere."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.night-light.gschema.xml:19
msgid "Use the sunrise and sunset"
msgstr "Bruk soloppgang og solnedgang"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.night-light.gschema.xml:20
msgid ""
"Calculate the sunrise and sunset times automatically, from the current "
"location."
msgstr ""
"Regn ut soloppgangs- og solnedgangstider automatisk, fra nåværende posisjon."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.night-light.gschema.xml:25
#, fuzzy
msgid "The start time's hour"
msgstr "Valgt time for oppstart"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.night-light.gschema.xml:26
#, fuzzy
msgid "When “schedule-automatic” is disabled, use this start time in hours."
msgstr "Når «planlegg automatisk» er av, utføres oppstart denne timen."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.night-light.gschema.xml:32
#, fuzzy
msgid "The start time's minute"
msgstr "Valgt minutt i time for oppstart"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.night-light.gschema.xml:33
#, fuzzy
msgid "When “schedule-automatic” is disabled, use this start time minutes."
msgstr "Når «planlegg automatisk» er avslått, utføres oppstart dette minuttet."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.night-light.gschema.xml:39
msgid "The end time's hour"
msgstr "Valgt time for sluttføring"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.night-light.gschema.xml:40
msgid "When “schedule-automatic” is disabled, use this end time in hours."
msgstr "Når «planlegg automatisk» er av, utføres sluttføring denne timen."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.night-light.gschema.xml:46
#, fuzzy
msgid "The end time's minute"
msgstr "Valgt minutt i time for sluttføring"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.night-light.gschema.xml:47
#, fuzzy
msgid "When “schedule-automatic” is disabled, use this end time minutes."
msgstr "Når «planlegg automatisk» er av, utføres oppstart dette minuttet."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:13
msgid "Power button action"
msgstr "Handling for av/på-knapp"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:14
#, fuzzy
msgid ""
"The action to take when the system power button is pressed. This action is "
"hard-coded (and the setting ignored) on virtual machines (power off) and "
"tablets (suspend)."
msgstr ""
"Handling å utføre når av/på-knappen trykkes. Handlingen er hardkodet (og "
"innstillingen blir ignorert) på virtuelle maskiner (slå av) og nettbrett "
"(hvilemodus)."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:19
msgid "Enable power-saver profile when battery is low"
msgstr "Skru på batterisparingsprofil ved lite gjenværende batterinivå"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:20
msgid ""
"Automatically enable the \"power-saver\" profile using power-profiles-daemon "
"if the battery is low."
msgstr ""
"Skru på «effektsparing»-profil automatisk ved bruk av "
"effektstyringsprofilnissen hvis gjenværende batterinivå er lavt."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:25
msgid "Dim the screen after a period of inactivity"
msgstr "Demp skjermens lysstyrke etter en tids inaktivitet"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:26
msgid "If the screen should be dimmed to save power when the computer is idle."
msgstr ""
"Hvorvidt skjermen skal ha mindre lysstyrke når datamaskinen er inaktiv for å "
"spare strøm."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:31
#, fuzzy
msgid "Whether to blank screen when session is idle"
msgstr "Hvorvidt skjermen skal blankes ut ved øktlediggang"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:32
#, fuzzy
msgid ""
"If the screen should be blanked to save power when the computer is idle."
msgstr "Hvorvidt skjermen skal blankes ut for å spare strøm ved lediggang."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:37
#, fuzzy
msgid "Time before session is considered idle"
msgstr "Tid før økten går inn i lediggang"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:38
#, fuzzy
msgid ""
"The number of minutes of inactivity before the session is considered idle."
msgstr "Antall minutters inaktivitet før økten antas å være inaktiv."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:43
#, fuzzy
msgid "Whether to suspend when inactive and using battery"
msgstr "Hvorvidt hvilemodus skal brukes ved inaktivitet på batteridrift"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:48
msgid "Sleep timeout for computer when on battery"
msgstr "Dvalgangstidsavbrudd for datamaskinen når den går på batteri"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:49
#, fuzzy
msgid ""
"The amount of time in minutes the computer on battery power needs to be "
"inactive before it goes to sleep. A value of 0 means never."
msgstr ""
"Antall minutter datamaskinen må være inaktiv på batteridrift før dvalemodus "
"igangsettes. «0» betyr aldri."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:54
#, fuzzy
msgid "Whether to suspend when inactive and plugged in"
msgstr "Hvorvidt hvilemodus skal brukes ved inaktivitet når innplugget"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:59
#, fuzzy
msgid "Sleep timeout for computer when plugged in"
msgstr "Dvalemodustidsavbrudd for datamaskinen når innplugget"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.power.gschema.xml:60
#, fuzzy
msgid ""
"The amount of time in minutes the computer on AC power needs to be inactive "
"before it goes to sleep. A value of 0 means never."
msgstr ""
"Antall minutter datamaskinen må være inaktiv innplugget før dvalemodus "
"igangsettes. «0» betyr aldri."

#: data/gschemas/io.github.realmazharhussain.GdmSettings.tools.gschema.xml:7
#, fuzzy
msgid "Whether to apply top bar tweaks to extracted theme"
msgstr "Hvorvidt justeringer av toppfelt skal brukes i utpakket drakt"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.top-bar.gschema.xml:7
msgid "Whether to disable top bar arrows"
msgstr "Hvorvidt piler skal skrus av i toppfeltet"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.top-bar.gschema.xml:12
#, fuzzy
msgid "Whether to disable top bar rounded corners"
msgstr "Hvorvidt avrundede hjørner skal skrus av i toppfeltet"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.top-bar.gschema.xml:17
#, fuzzy
msgid "Whether to change top bar text color"
msgstr "Hvorvidt tekstfargen skal endres i toppfeltet"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.top-bar.gschema.xml:22
msgid "Top bar text color"
msgstr "Tekstfarge for toppfelt"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.top-bar.gschema.xml:27
#, fuzzy
msgid "Whether to change top bar background"
msgstr "Hvorvidt bakgrunnen skal endres i toppfeltet"

#: data/gschemas/io.github.realmazharhussain.GdmSettings.top-bar.gschema.xml:32
#, fuzzy
msgid "Top bar background color"
msgstr "Bakgrunnsfarge for toppfelt"

#: data/io.github.realmazharhussain.GdmSettings.metainfo.xml.in:12
#: data/io.github.realmazharhussain.GdmSettings.desktop.in:4 src/info.py.in:4
#: src/window.py:66
msgid "Login Manager Settings"
msgstr "Innstillinger for innloggingsbehandler"

#: data/io.github.realmazharhussain.GdmSettings.metainfo.xml.in:13
#: data/io.github.realmazharhussain.GdmSettings.desktop.in:5
msgid "Customize your login screen"
msgstr "Tilpass innloggingsskjermen din"

#. Translators: GDM is an acronym for GNOME Display Manager (which is also GNOME's Login Manager).
#: data/io.github.realmazharhussain.GdmSettings.metainfo.xml.in:17
msgid ""
"Change GDM Settings; Apply theme and background, change cursor theme, icon "
"theme and night light settings, among other things."
msgstr ""
"Endre GDM-innstillinger; Bruk drakt og bakgrunn, endre pekerdrakt, "
"ikondrakt, og nattlysinnstillinger, med mer."

#: data/io.github.realmazharhussain.GdmSettings.metainfo.xml.in:55
msgid "Mazhar Hussain"
msgstr "Mazhar Hussain"

#: data/io.github.realmazharhussain.GdmSettings.metainfo.xml.in:100
msgid "Fixed: Fails to run on PureOS"
msgstr ""

#: data/io.github.realmazharhussain.GdmSettings.metainfo.xml.in:112
msgid "<em>New Options</em>"
msgstr "<em>Nye alternativer</em>"

#: data/io.github.realmazharhussain.GdmSettings.metainfo.xml.in:114
msgid "Option to disable accessiblitly menu when not being used"
msgstr "Mulighet til å skru av tilgjengelighetsmenyen når den ikke brukes"

#: data/io.github.realmazharhussain.GdmSettings.metainfo.xml.in:115
msgid "Option to change cursor size"
msgstr "Mulighet til å endre pekerstørrelse"

#: data/io.github.realmazharhussain.GdmSettings.metainfo.xml.in:116
msgid "A one-time donation request"
msgstr "En engangsforespørsel om donasjon"

#: data/io.github.realmazharhussain.GdmSettings.metainfo.xml.in:117
msgid "Donate option in hamburger menu"
msgstr "Donasjonsalternativ i hamburgermenyen"

#: data/io.github.realmazharhussain.GdmSettings.metainfo.xml.in:119
msgid "<em>Behavior Changes</em>"
msgstr "<em>Adferdsendringer</em>"

#: data/io.github.realmazharhussain.GdmSettings.metainfo.xml.in:121
msgid "Proper names are shown for themes instead of name of their directory"
msgstr "Ordentlige navn vist for drakter istedenfor deres mappenavn"

#: data/io.github.realmazharhussain.GdmSettings.metainfo.xml.in:122
msgid "Cursor only themes are not presented when choosing icon theme"
msgstr "Drakter kun for pekerendringer vises ikke ved valg av ikondrakt"

#: src/about.py:11 src/about.py:82
msgctxt "Name of Developer"
msgid "Mazhar Hussain"
msgstr "Mazhar Hussain"

#: src/about.py:12
msgctxt "Name of Artist"
msgid "Thales Bindá"
msgstr "Thales Bindá"

#: src/about.py:83
#, fuzzy
#| msgid "Copyright 2021-2022 Mazhar Hussain"
msgid "Copyright 2021-2023 Mazhar Hussain"
msgstr "Opphavsrett 2021–2023 Mazhar Hussain"

#. Translators: Do not translate this string. Put your info here in the form
#. 'name <email>' including < and > but not quotes.
#: src/about.py:95
msgid "translator-credits"
msgstr "Allan Nordhøy <epost@anotheragency.no>"

#: src/app.py:58
msgid "Show application version"
msgstr "Vis programversjon"

#: src/app.py:59
#, fuzzy
msgid "Set verbosity level manually (from 0 to 5)"
msgstr "Sett detaljnivå manuelt (fra 0 til 5)"

#: src/app.py:60
msgctxt "Argument of --verbosity option"
msgid "LEVEL"
msgstr "Nivå"

#: src/app.py:61
#, fuzzy
msgid "Enable verbose mode (alias of --verbosity=5)"
msgstr "Skru på ordrikt modus (alias for «--verbosity=5»)"

#: src/app.py:62
msgid "Enable quiet mode (alias of --verbosity=0)"
msgstr "Skru på stille modus (alias for «--verbosity=0»)"

#: src/app.py:82
#, fuzzy, python-brace-format
msgid "Invalid verbosity level {level}. Must be 0, 5, or in-between."
msgstr "Ugyldig detaljnivå «{level}», må være fra 1-5."

#: src/app.py:164
#, fuzzy
msgid ""
"Following programs are required to be installed <b>on the host system</b> "
"for this app to function properly but they are not installed on the host "
"system."
msgstr ""
"Følgende programmer må være installert <b>på vertssystemet</b> for at "
"programmet skal fungerer rett, men de er ikke installert på vertssystemet."

#: src/app.py:168
#, fuzzy
msgid ""
"This app requires the following software to function properly but they are "
"not installed."
msgstr ""
"Dette programmet krever følgende programvare for å fungere, men de er ikke "
"installert."

#: src/app.py:177
msgctxt "Missing Dependency"
msgid "<b>GDM</b>"
msgstr "<b>GDM</b>"

#: src/app.py:183
msgctxt "Missing Dependency"
msgid "<b>Polkit</b>"
msgstr "<b>Polkit</b>"

#. Translators: Keep '<a href="{url}">' and '</a>' as is. The words between them will become
#. a link to '{url}' and '{url}' will be replaced by a real URL during program execution.
#: src/app.py:191
#, fuzzy, python-brace-format
msgid ""
"Click <a href=\"{url}\">here</a> for instructions on how to install these "
"dependencies on your system."
msgstr ""
"Klikk <a href=\"{url}\">her</a> for instruks om hvordan du installerer disse "
"avhengighetene på systemet ditt."

#: src/app.py:198
msgid "Missing Dependencies"
msgstr "Manglende avhengigheter"

#: src/app.py:203 src/pages/display.py:107
msgid "OK"
msgstr "OK"

#: src/app.py:209
msgid "Donation Request"
msgstr "Donasjonsforespørsel"

#: src/app.py:211
msgid ""
"This app is and will always remain Open Source/Libre and free of cost. "
"However, You can show some love by donating to the developer.\n"
"❤️\n"
"I would really appreciate it."
msgstr ""
"Dette programmet vil alltid forbli fritt og gratis. Du kan sende en "
"påskjønnelse til utvikleren.\n"
"❤️\n"
"Jeg setter stor pris på det."

#: src/app.py:219
msgid "Not Interested"
msgstr "Ikke interessert"

#: src/app.py:220 src/resources/ui/main-window.blp:15
msgid "Donate"
msgstr "Doner"

#: src/app.py:257
msgid "Settings reloaded"
msgstr "Innstillinger ble gjeninnlastet"

#: src/app.py:264
#, fuzzy
msgid "Session settings loaded successfully"
msgstr "Øktinnstillinger ble innlastet"

#: src/app.py:268
#, fuzzy
msgid "Load session settings is NOT supported in Flatpak version"
msgstr "Innlasting av øktinnstillinger støttes ikke i Flatpak-versjonen"

#: src/app.py:281
#, fuzzy
msgid "Reset settings successfully"
msgstr "Innstillinger tilbakestilt"

#: src/app.py:283
#, fuzzy
msgid "Failed to reset settings"
msgstr "Klarte ikke å tilbakestille innstillingene"

#: src/app.py:302
msgid "Import"
msgstr "Importer"

#: src/app.py:323
#, fuzzy
msgid "Settings were successfully imported from file"
msgstr "Innstillinger importert fra fil"

#: src/app.py:325
msgid "Failed to import. File is invalid"
msgstr "Kunne ikke importere. Filen er ugyldig."

#: src/app.py:356
msgid "Settings were successfully exported"
msgstr "Innstillingene ble importert"

#: src/app.py:358
#, fuzzy
msgid "Failed to export. Permission denied"
msgstr "Klarte ikke å eksportere. Mangler tilgang."

#: src/app.py:360
#, fuzzy
msgid "Failed to export. A directory with that name already exists"
msgstr "Klarte ikke å eksportere. En mappe med det navnet finnes allerede."

#: src/lib/file_choosers.py:18
msgid "Choose File"
msgstr "Velg fil"

#: src/lib/file_choosers.py:25
msgid "(None)"
msgstr "(Ingen)"

#: src/lib/file_choosers.py:51
#, fuzzy
#| msgid "Choose File"
msgid "Choose"
msgstr "Velg fil"

#: src/lib/file_choosers.py:110
#, fuzzy
#| msgid "Image"
msgid "Images"
msgstr "Bilde"

#: src/lib/file_choosers.py:114
msgid "All Files"
msgstr "Alle filer"

#: src/lib/file_choosers.py:119
#, fuzzy
#| msgid "Choose File"
msgid "Choose Image"
msgstr "Velg fil"

#: src/pages/display.py:85
msgid "Applied current display settings"
msgstr "Brukte nåværende skjerminnstillinger"

#: src/pages/display.py:87
msgid "Failed to apply current display settings"
msgstr "Klarte ikke å bruke nåværende skjerminnstillinger"

#: src/pages/display.py:93
#, fuzzy
msgid ""
"'$XDG_CONFIG_HOME/monitors.xml' file is required to apply current display "
"settings but it does not exist.\n"
"\n"
"In order to create that file automatically, open system 'Settings' and "
"change some display options there."
msgstr ""
"«$XDG_CONFIG_HOME/monitors.xml»-filen kreves for å bruke nåværende "
"skjerminnstillinger, men den finnes ikke.\n"
"\n"
"For å opprette den filen automatisk kan du åpne «Innstillinger» og endre "
"noen av skjermalternativene der."

#: src/pages/display.py:102
#, fuzzy
msgid "Monitor Settings Not Found"
msgstr "Fant ikke skjerminnstillinger"

#. Translators: Do not translate '{folder}' and '{name}'. Keep these as they are.
#. They will be replaced by an actual folder path and theme name during runtime.
#: src/pages/tools.py:49
#, python-brace-format
msgid "Default shell theme extracted to '{folder}' as '{name}'"
msgstr "Forvalgt skalldrakt pakket ut til «{folder}» som «{name}»"

#: src/pages/tools.py:52
msgid "Failed to extract default theme"
msgstr "Klarte ikke å pakke ut forvalgt drakt"

#: src/resources/ui/appearance-page.blp:9
msgid "Themes"
msgstr "Drakter"

#: src/resources/ui/appearance-page.blp:11
msgid "Shell"
msgstr "Skall"

#: src/resources/ui/appearance-page.blp:12
msgid "Icons"
msgstr "Ikoner"

#: src/resources/ui/appearance-page.blp:13
msgid "Cursor"
msgstr "Peker"

#: src/resources/ui/appearance-page.blp:17
msgid "Background"
msgstr "Bakgrunn"

#: src/resources/ui/appearance-page.blp:20
msgid "Type"
msgstr "Type"

#: src/resources/ui/appearance-page.blp:21
msgid "Default"
msgstr "Forvalg"

#: src/resources/ui/appearance-page.blp:21
#: src/resources/ui/appearance-page.blp:26
msgid "Image"
msgstr "Bilde"

#: src/resources/ui/appearance-page.blp:21
#: src/resources/ui/appearance-page.blp:31
msgid "Color"
msgstr "Farge"

#: src/resources/ui/appearance-page.blp:27
msgid "The image to use as background"
msgstr "Bilde å bruke som bakgrunn"

#: src/resources/ui/appearance-page.blp:32
msgid "The color to use as background"
msgstr "Farge å bruke som bakgrunn"

#: src/resources/ui/display-page.blp:12
msgid "Apply current display settings"
msgstr "Bruk nåværende skjerminnstillinger"

#: src/resources/ui/display-page.blp:13
msgid "Resolution, refresh rate, monitor positions, etc."
msgstr "Oppløsning, oppdateringsfrekvens, skjermposisjoner, osv."

#: src/resources/ui/display-page.blp:16 src/resources/ui/main-window.blp:43
msgid "Apply"
msgstr "Bruk"

#: src/resources/ui/display-page.blp:21
msgid "Night Light"
msgstr "Nattlys"

#: src/resources/ui/display-page.blp:24
#: src/resources/ui/login-screen-page.blp:40
msgid "Enable"
msgstr "Skru på"

#: src/resources/ui/display-page.blp:28
msgid "Schedule"
msgstr "Tidsplan"

#: src/resources/ui/display-page.blp:29
msgid "Sunset to Sunrise"
msgstr "Solnedgang til soloppgang"

#: src/resources/ui/display-page.blp:29
msgid "Manual Schedule"
msgstr "Manuell plan"

#: src/resources/ui/display-page.blp:33
msgid "Times"
msgstr "Tider"

#. Translators: This is part of the timespan in which Night Light is active, e. g. "From 21:00 To 06:00".
#: src/resources/ui/display-page.blp:36
msgid "From"
msgstr "Fra"

#. Translators: This is part of the timespan in which Night Light is active, e. g. "From 21:00 To 06:00".
#: src/resources/ui/display-page.blp:60
msgid "To"
msgstr "Til"

#: src/resources/ui/display-page.blp:84
msgid "Color Temperature"
msgstr "Fargetemperatur"

#: src/resources/ui/fonts-page.blp:12
msgid "Font"
msgstr "Skrift"

#: src/resources/ui/fonts-page.blp:21
msgid "Antialiasing"
msgstr "Kantutjevning"

#: src/resources/ui/fonts-page.blp:23
msgid "Standard"
msgstr "Forvalg"

#: src/resources/ui/fonts-page.blp:24
#, fuzzy
msgid "Subpixel"
msgstr "Underpiksel"

#: src/resources/ui/fonts-page.blp:25 src/resources/ui/fonts-page.blp:35
msgid "None"
msgstr "Ingen"

#: src/resources/ui/fonts-page.blp:30
msgid "Hinting"
msgstr "Hinting"

#: src/resources/ui/fonts-page.blp:32
msgid "Full"
msgstr "Full"

#: src/resources/ui/fonts-page.blp:33
msgid "Medium"
msgstr "Middels"

#: src/resources/ui/fonts-page.blp:34
msgid "Slight"
msgstr "Lett"

#: src/resources/ui/fonts-page.blp:40
msgid "Scaling Factor"
msgstr "Skaleringsfaktor"

#: src/resources/ui/main-window.blp:6
msgid "Refresh"
msgstr "Gjenoppfrisk"

#: src/resources/ui/main-window.blp:7
msgid "Load session settings"
msgstr "Last inn øktinnstillinger"

#: src/resources/ui/main-window.blp:8
msgid "Reset settings"
msgstr "Tilbakestill innstillinger"

#: src/resources/ui/main-window.blp:11
msgid "Import from file"
msgstr "Importer fra fil"

#: src/resources/ui/main-window.blp:12
msgid "Export to file"
msgstr "Eksporter til fil"

#: src/resources/ui/main-window.blp:18
msgid "About"
msgstr "Om"

#: src/resources/ui/main-window.blp:19
msgid "Quit"
msgstr "Avslutt"

#: src/resources/ui/login-screen-page.blp:12
#, fuzzy
msgid "Disable Restart Buttons"
msgstr "Skru av omstartsknapper"

#: src/resources/ui/login-screen-page.blp:16
msgid "Disable User List"
msgstr "Skru av brukerliste"

#: src/resources/ui/login-screen-page.blp:20
msgid "Enable Logo"
msgstr "Skru på logo"

#: src/resources/ui/login-screen-page.blp:21
msgid "Whether to show a logo below user list"
msgstr "Hvorvidt en logo skal vises under brukerlisten"

#: src/resources/ui/login-screen-page.blp:30
msgid "Logo"
msgstr "Logo"

#: src/resources/ui/login-screen-page.blp:31
msgid "The image to show below user list"
msgstr "Bilde å vise under brukerlisten"

#: src/resources/ui/login-screen-page.blp:37
msgid "Welcome Message"
msgstr "Velkomstmelding"

#: src/resources/ui/login-screen-page.blp:44
msgid "Enlarge"
msgstr "Forstørr"

#: src/resources/ui/login-screen-page.blp:49
msgid "Message"
msgstr "Melding"

#: src/resources/ui/pointing-page.blp:10
#, fuzzy
#| msgid "Cursor"
msgid "Cursor Size"
msgstr "Peker"

#: src/resources/ui/pointing-page.blp:15
msgctxt "Cursor Size"
msgid "Default"
msgstr "Forvalg"

#: src/resources/ui/pointing-page.blp:16
#, fuzzy
#| msgid "Medium"
msgctxt "Cursor Size"
msgid "Medium"
msgstr "Middels"

#: src/resources/ui/pointing-page.blp:17
msgctxt "Cursor Size"
msgid "Large"
msgstr "Stor"

#: src/resources/ui/pointing-page.blp:18
msgctxt "Cursor Size"
msgid "Larger"
msgstr "Større"

#: src/resources/ui/pointing-page.blp:19
msgctxt "Cursor Size"
msgid "Largest"
msgstr "Størst"

#: src/resources/ui/pointing-page.blp:25
msgid "Mouse"
msgstr "Mus"

#: src/resources/ui/pointing-page.blp:28
msgid "Pointer Acceleration"
msgstr "Pekerakselerasjon"

#: src/resources/ui/pointing-page.blp:30
msgid "Automatic"
msgstr "Automatisk"

#: src/resources/ui/pointing-page.blp:31
msgid "Disabled"
msgstr "Avskrudd"

#: src/resources/ui/pointing-page.blp:32
msgid "Enabled"
msgstr "Påskrudd"

#: src/resources/ui/pointing-page.blp:37 src/resources/ui/pointing-page.blp:59
msgid "Natural Scrolling"
msgstr "Naturlig rulling"

#: src/resources/ui/pointing-page.blp:38 src/resources/ui/pointing-page.blp:60
msgid "Scrolling moves the content, not the view."
msgstr "Rulling flytter innholdet, ikke visningen."

#: src/resources/ui/pointing-page.blp:42 src/resources/ui/pointing-page.blp:73
msgid "Pointer Speed"
msgstr "Pekerhastighet"

#: src/resources/ui/pointing-page.blp:52
msgid "Touchpad"
msgstr "Pekeflate"

#: src/resources/ui/pointing-page.blp:55
msgid "Tap to Click"
msgstr "Trykk for å klikke"

#: src/resources/ui/pointing-page.blp:64
msgid "Two-finger Scrolling"
msgstr "Tofingerrulling"

#: src/resources/ui/pointing-page.blp:65
msgid "Scroll by swiping with two fingers."
msgstr "Rull ved å dra med to fingre."

#: src/resources/ui/pointing-page.blp:69
msgid "Disable while Typing"
msgstr "Skru av under skriving"

#: src/resources/ui/power-page.blp:12
msgid "Power Button Behavior"
msgstr "Oppførsel for av/på-knapp"

#: src/resources/ui/power-page.blp:13
msgid "Do Nothing"
msgstr "Ikke gjør noe"

#: src/resources/ui/power-page.blp:13
msgid "Suspend"
msgstr "Hvilemodus"

#: src/resources/ui/power-page.blp:13
msgid "Hibernate"
msgstr "Dvalemodus"

#: src/resources/ui/power-page.blp:13
msgid "Ask"
msgstr "Spør"

#: src/resources/ui/power-page.blp:18
msgid "Automatic Power Saver"
msgstr "Automatisk strømsparing"

#: src/resources/ui/power-page.blp:19
msgid "Enables power saver mode when battery is low."
msgstr "Skru på strømsparingsmodus når batterinivået er lavt"

#: src/resources/ui/power-page.blp:23
msgid "Dim Screen"
msgstr "Senk skjermens lysstyrke"

#: src/resources/ui/power-page.blp:24
msgid "Reduces the screen brightness when the computer is inactive."
msgstr "Reduserer skjermlysstyrken når datamaskinen er inaktiv."

#: src/resources/ui/power-page.blp:28
msgid "Turn Screen Off"
msgstr "Skru av skjerm"

#: src/resources/ui/power-page.blp:29
#, fuzzy
msgid "Whether to turn the screen off after a period of inactivity."
msgstr "Hvorvidt skjermen skal skrus av etter en stunds inaktivitet."

#: src/resources/ui/power-page.blp:38 src/resources/ui/power-page.blp:70
#: src/resources/ui/power-page.blp:97
msgid "Delay"
msgstr "Forsinkelse"

#: src/resources/ui/power-page.blp:39
#, fuzzy
msgid "Time in minutes after which screen will turn off."
msgstr "Antall minutter til skjermen skrur seg av."

#: src/resources/ui/power-page.blp:57
msgid "Automatic Suspend"
msgstr "Automatisk hvilemodus"

#: src/resources/ui/power-page.blp:58
msgid "Pauses the computer after a period of inactivity."
msgstr "Setter datamaskinen på pause etter en tids inaktivitet."

#: src/resources/ui/power-page.blp:61
msgid "On Battery Power"
msgstr "På batteridrift"

#: src/resources/ui/power-page.blp:71 src/resources/ui/power-page.blp:98
#, fuzzy
msgid "Time in minutes after which the computer will suspend."
msgstr "Antall minutter etterpå datamaskinen går i hvilemodus."

#: src/resources/ui/power-page.blp:88
msgid "When Plugged In"
msgstr "Når innplugget"

#: src/resources/ui/sound-page.blp:11
msgid "Theme"
msgstr "Drakt"

#: src/resources/ui/sound-page.blp:14
msgid "Over Amplification"
msgstr "Overforsterkning"

#: src/resources/ui/sound-page.blp:15
msgid "Allow raising volume above 100%"
msgstr "Tillat økning av lydstyrken til over 100%"

#: src/resources/ui/sound-page.blp:19
msgid "Event Sounds"
msgstr "Begivenhetslyder"

#: src/resources/ui/sound-page.blp:23
msgid "Input Feedback Sounds"
msgstr "Inndatatilbakemeldingslyder"

#: src/resources/ui/tools-page.blp:9
msgid "Default Shell Theme"
msgstr "Forvalgt skalldrakt"

#: src/resources/ui/tools-page.blp:12
msgid "Include Top Bar Tweaks"
msgstr "Inkluder toppfeltsjusteringer"

#: src/resources/ui/tools-page.blp:16
msgid "Extract default shell theme"
msgstr "Pakk ut forvalgt skalldrakt"

#: src/resources/ui/tools-page.blp:19
msgid "Extract"
msgstr "Pakk ut"

#: src/resources/ui/top-bar-page.blp:9
msgid "Tweaks"
msgstr "Justeringer"

#: src/resources/ui/top-bar-page.blp:12
msgid "Change Text Color"
msgstr "Endre tekstfarge"

#: src/resources/ui/top-bar-page.blp:20
msgid "Text Color"
msgstr "Tekstfarge"

#: src/resources/ui/top-bar-page.blp:21
msgid "The color of text in top bar"
msgstr "Tekstfargen i toppfeltet"

#: src/resources/ui/top-bar-page.blp:32
msgid "Change Background Color"
msgstr "Endre bakgrunnsfarge"

#: src/resources/ui/top-bar-page.blp:41
msgid "The color of top bar background"
msgstr "Farge for toppfeltets bakgrunn"

#: src/resources/ui/top-bar-page.blp:52
msgid "Disable Arrows"
msgstr "Skru av piler"

#: src/resources/ui/top-bar-page.blp:56
msgid "Disable Rounded Corners"
msgstr "Skru av avrundede hjørner"

#: src/resources/ui/top-bar-page.blp:61
msgid "Accessibility"
msgstr "Tilgjengelighet"

#: src/resources/ui/top-bar-page.blp:64
msgid "Always Show Accessibility Menu"
msgstr "Alltid vis tilgjengelighetsmenyen"

#: src/resources/ui/top-bar-page.blp:69
msgid "Clock"
msgstr "Klokke"

#: src/resources/ui/top-bar-page.blp:72
msgid "Show Weekday"
msgstr "Vis ukedag"

#: src/resources/ui/top-bar-page.blp:76
msgid "Show Seconds"
msgstr "Vis sekunder"

#: src/resources/ui/top-bar-page.blp:80
msgid "Time Format"
msgstr "Tidsformat"

#: src/resources/ui/top-bar-page.blp:81
msgid "AM/PM"
msgstr "AM/PM"

#: src/resources/ui/top-bar-page.blp:81
#, fuzzy
msgid "24 Hours"
msgstr "24-timersklokke"

#: src/resources/ui/top-bar-page.blp:86 src/window.py:118
msgid "Power"
msgstr "Effekt"

#: src/resources/ui/top-bar-page.blp:89
msgid "Show battery percentage"
msgstr "Vis batteriprosent"

#: src/settings.py:80
#, python-brace-format
msgid "Exporting to file '{filename}'"
msgstr "Eksporterer til filen «{filename}»"

#: src/settings.py:85
#, fuzzy, python-brace-format
msgid "Cannot write to file '{filename}'. Permission denied"
msgstr "Kan ikke skrive til filen «{filename}». Mangler tilgang."

#: src/settings.py:89
#, python-brace-format
msgid ""
"Cannot write to file '{filename}'. A directory with the same name already "
"exists"
msgstr ""
"Kan ikke skrive til filen «{filename}». En mappe med samme navn finnes "
"allerede."

#: src/settings.py:94
msgid "Exporting to standard output"
msgstr "Eksporterer til forvalgt utdata"

#: src/settings.py:102
#, python-brace-format
msgid "Importing from file '{filename}'"
msgstr "Importerer fra filen «{filename}»."

#: src/settings.py:105
msgid "Importing from standard input"
msgstr "Importerer fra forvalgt inndata"

#: src/settings.py:108
msgid "Failed to parse import file"
msgstr "Kunne ikke tolke importfil"

#: src/settings.py:111
msgid "Failed to read import file. Not encoded in UTF-8"
msgstr "Kunne ikke lese importfil. Ikke i UTF-8-koding"

#: src/settings.py:117
#, fuzzy, python-brace-format
msgid "Imported file does not have section '{section_name}'"
msgstr "Importert fil mangler delen «{section_name}»"

#: src/settings.py:122
#, python-brace-format
msgid ""
"Imported file does not have key '{key_name}' in section '{section_name}'"
msgstr "Importert fil har ikke nøkkelen «{key_name}» i «{section_name}»-delen"

#: src/settings.py:313
msgid "Backing up default shell theme …"
msgstr "Sikkerhetskopierer forvalgt skalldrakt …"

#: src/settings.py:365
#, python-brace-format
msgid "Applying GResource settings for {distro_name} …"
msgstr "Bruker GResource-innstilling for {distro_name} …"

#: src/settings.py:386
msgid "Ubuntu"
msgstr "Ubuntu"

#: src/settings.py:388
msgid "generic system"
msgstr "generisk system"

#: src/settings.py:620
msgctxt "Command-line output"
msgid "Resetting GResource settings for Ubuntu …"
msgstr "Tilbakestiller GResource-innstillinger for Ubuntu …"

#: src/settings.py:627
msgctxt "Command-line output"
msgid "Resetting GResource settings for non-Ubuntu systems …"
msgstr ""
"Tilbakestiller GResource-innstillinger for systemer som ikke er Ubuntu …"

#: src/window.py:111
msgid "Appearance"
msgstr "Utseende"

#: src/window.py:112
msgid "Fonts"
msgstr "Skrifter"

#: src/window.py:113
msgid "Top Bar"
msgstr "Toppfelt"

#: src/window.py:114
msgid "Sound"
msgstr "Lyd"

#: src/window.py:115
msgid "Mouse & Touchpad"
msgstr "Mus og pekeflate"

#: src/window.py:116
msgid "Display"
msgstr "Skjerm"

#: src/window.py:117
msgid "Login Screen"
msgstr "Innloggingsskjerm"

#: src/window.py:119
msgid "Tools"
msgstr "Verktøy"

#: src/window.py:137
#, fuzzy
msgid "Settings applied successfully"
msgstr "Innstillinger iverksatt"

#: src/window.py:141
#, fuzzy
msgid "Failed to apply settings"
msgstr "Klarte ikke å bruke innstillinger"

#: src/window.py:145
#, fuzzy
msgid ""
"Didn't apply. Chosen background image does not exist anymore. Please! choose "
"again."
msgstr "Hadde ingen innvirkning fordi bakgrunnsbildet ikke finnes. Velg en ny."

#: src/window.py:149
#, fuzzy
msgid ""
"Didn't apply. Chosen logo image does not exist anymore. Please! choose again."
msgstr "Hadde ingen innvirkning fordi valgt logo ikke finnes. Velg en ny."

#: src/window.py:155
msgid "The system may start to look weird/buggy until you re-login or reboot."
msgstr ""
"Systemet kan begynne å se rart/feilaktig ut til du logger inn igjen eller "
"utfører omstart."

#: src/window.py:160
msgid "Log Out?"
msgstr "Logg ut?"

#: src/window.py:165
msgid "Cancel"
msgstr "Avbryt"

#: src/window.py:166
msgid "Log Out"
msgstr "Logg ut"

#~ msgctxt "Command-line output"
#~ msgid "Applying GResource settings for non-Ubuntu systems …"
#~ msgstr "Bruker GResource-innstillinger for systemer som ikke er Ubuntu …"

#~ msgid "Added power settings"
#~ msgstr "La til effektstyringsinnstillinger"

#~ msgid "Added options to import/export settings in the global menu"
#~ msgstr ""
#~ "La til alternativer for import/eksport av innstillinger i den globale "
#~ "menyen"

#~ msgid "Added an option to enlarge welcome message"
#~ msgstr "La til alternativ for å gjøre velkomstmeldingen større"

#~ msgid "Added color to command-line output"
#~ msgstr "La til farge for kommandolinjeutdata"

#~ msgid "Turned main window adaptive"
#~ msgstr "Gjorde hovedvinduet tilpasningsdyktig"

#~ msgid "Turned pop-up dialogs adaptive"
#~ msgstr "Gjorde oppsprettsdialoger tilpasningsdyktige"

#, fuzzy
#~ msgid "Updated \"about\" window to the new style"
#~ msgstr "Oppdatert «Om»-vindu til ny stil"

#~ msgid "Improved style of welcome message input field"
#~ msgstr "Forbedret stil for velkomstmeldingens inndatafelt"

#~ msgid "Renamed sidebar section \"Miscellaneous\" to \"Login Screen\""
#~ msgstr "Endret sidenavnsdelen «Ymse» til «Innloggingsskjerm»"

#~ msgid ""
#~ "Renamed global menu option \"Import user settings\" to \"Load session "
#~ "settings\""
#~ msgstr ""
#~ "Endret globalt menyvalg «Importer brukerinnstillinger» til «Last inn "
#~ "øktinnstillinger»"

#, fuzzy
#~ msgid ""
#~ "The app, now, shows an error notification when trying to apply settings "
#~ "if logo is enabled and the chosen logo image does not exist on the system "
#~ "anymore"
#~ msgstr ""
#~ "Programmet viser nå en merknad om feil ved forsøk på å bruke "
#~ "innstillinger hvis logo er påslått og valgt logobildet ikke finnes på "
#~ "systemet."

#, fuzzy
#~ msgid ""
#~ "Fixed \"Apply Current Display Settings\" feature not working on some "
#~ "Ubuntu-based systems"
#~ msgstr ""
#~ "Fikset «Bruk nåværende skjerminnstillinger» på Ubuntu og lignende systemer"

#~ msgid "Fixed some text showing up untranslated even if translation existed"
#~ msgstr "Fikset noe tekst som ble vist uoversatt selv om de var oversatt"

#~ msgid "Fixed some typos"
#~ msgstr "Fikset noen skrivefeil"

#, fuzzy
#~| msgid "Standard"
#~ msgctxt "Cursor Size"
#~ msgid "Non-Standard"
#~ msgstr "Forvalg"

#, fuzzy
#~ msgid ""
#~ "Fixed \"Apply Current Display Settings\" feature not working on Ubuntu "
#~ "and Ubuntu-like systems"
#~ msgstr ""
#~ "Fikset «Bruk nåværende skjerminnstillinger» på Ubuntu og lignende systemer"

#~ msgid "Added:"
#~ msgstr "Tillagt:"

#~ msgid "Fixed:"
#~ msgstr "Fikset:"

#~ msgid "French"
#~ msgstr "Fransk"

#~ msgid "German"
#~ msgstr "Tysk"

#~ msgid "Spanish"
#~ msgstr "Spansk"

#~ msgid "Improved:"
#~ msgstr "Forbedret:"
